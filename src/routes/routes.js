const express = require('express');
const router = express.Router();
const Resource = require('../models/resource')


router.get('/', async (req, res) => {
    const Resources = await Resource.find();
    console.log(Resources);
    res.json(Resources);
});

router.get('/:id', async (req, res) => {
    const resource = await Resource.findById(req.params.id);
    res.json(resource);
})

router.post('/', async (req, res) =>{
    const {title, keys, description, source, resource_type, coverage_since, coverage_until} = req.body;
    const resource = new Resource ({title, keys, description, source, resource_type, coverage_since, coverage_until});
    await resource.save();
    console.log(resource);
    res.json({"status":'saved'});
});

router.put('/:id', async (req, res) => {
    const {title, keys, description, source, resource_type, coverage_since, coverage_until} = req.body;
    const newResource = {title, keys, description, source, resource_type, coverage_since, coverage_until};
    await Resource.findByIdAndUpdate(req.params.id, newResource);
    res.json({status : 'updated'});

})

router.delete('/:id', async(req, res) => {
    await Resource.findByIdAndRemove(req.params.id);
    res.json({status : 'deleted'})
})

module.exports = router