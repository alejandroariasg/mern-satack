const mongoose = require('mongoose');

const db_uri = 'mongodb://localhost/mern-recursos';

mongoose.connect(db_uri)
    .then(db => console.log("DB connected"))
    .catch(err => console.error(err));

module.exports = mongoose;