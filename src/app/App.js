import React, { Component } from 'react';



class App extends Component{
    
    constructor(){
        super();
        this.state = {
            title : '',
            keys : '',
            description : '',
            source : '',
            resource_type : '',
            coverage_since : '',
            coverage_until : '',

            resources : [],
            _id : '',
        }
        this.addResource = this.addResource.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    
    addResource(e){
        if(this.state._id){
            fetch(`/api/${this.state._id}`,{
                method : 'PUT',
                body : JSON.stringify(this.state),
                headers : {
                    'Accept' : 'application/json',
                    'Content-Type' : 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                M.toast({html : '¡Recurso actualizado!'});
                this.setState({title : '', keys : '', description : '', source : '', resource_type : '', coverage : '', _id : '', coverage_since : '', coverage_until : ''});
                this.fetchResources();
            })
        }else{
            fetch('/api', {
                method : 'POST',
                body : JSON.stringify(this.state),
                headers : {
                    'Accept' : 'application/json',
                    'Content-Type' : 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                M.toast({html : '¡Recurso guardado!'})
                this.setState({title : '', keys : '', description : '', source : '', resource_type : '', coverage : '', coverage_since : '', coverage_until : ''})
                this.fetchResources();
            })
            .catch(err => console.log(err));
        }
        e.preventDefault();
    }

    componentDidMount(){
        this.fetchResources();
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.datepicker');
            M.Datepicker.init(elems, {});
        });
        
    }

    fetchResources(){
        fetch('/api')
            .then(res => res.json())
            .then(data => {
                this.setState({resources : data});
            })
    }

    deleteResource(id){
        if(confirm('¿ Está segurio de querer elimnar este recurso ?')){
            fetch(`/api/${id}`, {
                method : 'DELETE',
                headers : {
                    'Accept' : 'application/json',
                    'Content-Type' : 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                M.toast({html : '¡Recurso eliminado!'})
                this.fetchResources();
            })
        }
    }

    editResource(id){
        fetch(`/api/${id}`)
        .then(res => res.json())
        .then(data => {            
            var curr_since = new Date(data.coverage_since,);
            curr_since.setDate(curr_since.getDate() + 0);
            var date_since = curr_since.toISOString().substr(0,10);

            var curr_until = new Date(data.coverage_until,);
            curr_until.setDate(curr_until.getDate() + 0);
            var date_until = curr_until.toISOString().substr(0,10);

            this.setState({
                title : data.title,
                keys : data.keys,
                description : data.description,
                source : data.source,
                resource_type : data.resource_type,
                coverage_since : date_since,
                coverage_until : date_until,
                _id : data._id
            });
        })
    }
    
    handleChange(e){
        const { name, value } = e.target;
        this.setState({
            [name] : value
        });
    }
    
    render(){
        return(
            <div>
                <nav className="light-blue darken-4">
                    <div className="container">
                        <a className="brand-logo" href="/">MERN Stack - Alejandro Arias Gallo</a>
                    </div>
                </nav>
                <div className="container">
                    <div className="row">
                        <div className="col s5">
                            <div className="card">
                                <div className="card-content">
                                    <span className="card-title">Registro de recursos</span>
                                    <form onSubmit={this.addResource}>
                                        <div className="row">
                                            <div className="input-field cold s12">
                                                <input type="text" name="title" placeholder="Título" onChange={this.handleChange} value={this.state.title}/>
                                            </div>
                                            <div className="input-field cold s12">
                                                <input type="text" name="keys" placeholder="Claves" onChange={this.handleChange} value={this.state.keys}/>
                                            </div>
                                            <div className="input-field cold s12">
                                                <textarea placeholder="Descripción" name="description" className="materialize-textarea" onChange={this.handleChange} value={this.state.description}></textarea>
                                            </div>
                                            <div className="input-field cold s12">
                                                <input type="text" name="source" placeholder="Fuente" onChange={this.handleChange} value={this.state.source}/>
                                            </div>
                                            <div className="input-field cold s12">
                                                <input type="text" name="resource_type" placeholder="Tipo del Recurso" onChange={this.handleChange} value={this.state.resource_type}/>
                                            </div>
                                            <div className="input-field cold s12">
                                                <label style={{position: 'relative'}}>Cobertura desde:</label>
                                                <input type="date" name= "coverage_since" onChange={this.handleChange} className="" value={this.state.coverage_since}/>
                                            </div>
                                            <div className="input-field cold s12">
                                                <label style={{position: 'relative'}}>Cobertura hasta:</label>
                                                <input type="date" name= "coverage_until" onChange={this.handleChange} className="" value={this.state.coverage_until}/>
                                            </div>
                                            
                                            
                                            <button type="submit" className="btn waves-effect light-blue darken-4" laceholder="Cobertura desde">Guardar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col s7">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Título</th>
                                        <th>Claves</th>
                                        <th>Descripción</th>
                                        <th>Fuente</th>
                                        <th>Tipo recurso</th>
                                        <th>Cobertura desde</th>
                                        <th>Cobertura hasta</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.resources.map(resource => {
                                            return(
                                                <tr key={resource._id}>
                                                    <td>{resource.title}</td>
                                                    <td>{resource.keys}</td>
                                                    <td>{resource.description}</td>
                                                    <td>{resource.source}</td>
                                                    <td>{resource.resource_type}</td>
                                                    <td>{(resource.coverage_since+"").toString().substr(0,10)}</td>
                                                    <td>{(resource.coverage_until+"").toString().substr(0,10)}</td>
                                                    <td>
                                                        <button className="btn light-blue darken-4" style={{margin : '4px'}} onClick={() =>this.deleteResource(resource._id)}>
                                                            <i className="material-icons">delete</i>
                                                        </button>
                                                        <button className="btn light-blue" style={{margin : '4px'}}  onClick={() =>this.editResource(resource._id)}>
                                                            <i className="material-icons">edit</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;