const express = require('express');
const morgan = require('morgan');
const path = require('path');
const { mongoose } = require ('./database');

const app = express();

app.set('port', process.env.PORT || 3000);

// Middlewares
app.use(morgan('dev'));
app.use(express.json());

//Routes
app.use('/api', require('./routes/routes'));

//Satic files
app.use(express.static(path.join(__dirname, 'public')));

app.listen(app.get('port'), () =>{
    console.log(`Server port ${app.get('port')}`);
});