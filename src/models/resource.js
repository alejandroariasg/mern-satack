const mongoose = require('mongoose');
const { Schema } = mongoose;

const Resource = new Schema({
    title : { type : String,  require: true},
    keys : { type : String,  require: true},
    description : { type : String,  require: true},
    source : { type : String,  require: true},
    resource_type : { type : String,  require: true},
    coverage_since : { type : Date,  require: true},
    coverage_until : { type : Date,  require: true}
});

module.exports = mongoose.model('Resource', Resource);